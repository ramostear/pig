package com.github.pig.admin.common.util;

import com.github.pig.common.vo.ImageCode;
import org.springframework.stereotype.Component;
import com.github.pig.common.constant.SecurityConstants;
import org.springframework.web.context.request.ServletWebRequest;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * @author lengleng
 * @date 2017-12-18
 */
public class ImageCodeGenerator {

    public ImageCode generate(ServletWebRequest request) {
        BufferedImage image = new BufferedImage(SecurityConstants.DEFAULT_IMAGE_WIDTH, SecurityConstants.DEFAULT_IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);

        Graphics g = image.getGraphics();

        Random random = new Random();

        g.setColor(getRandColor(200, 250));
        g.fillRect(0, 0, SecurityConstants.DEFAULT_IMAGE_WIDTH, SecurityConstants.DEFAULT_IMAGE_HEIGHT);
        g.setFont(new Font("Times New Roman", Font.ITALIC, 20));
        g.setColor(getRandColor(160, 200));
        for (int i = 0; i < 155; i++) {
            int x = random.nextInt(SecurityConstants.DEFAULT_IMAGE_WIDTH);
            int y = random.nextInt(SecurityConstants.DEFAULT_IMAGE_HEIGHT);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            g.drawLine(x, y, x + xl, y + yl);
        }

        String sRand = "";
        for (int i = 0; i < SecurityConstants.DEFAULT_IMAGE_LENGTH; i++) {
            String rand = String.valueOf(random.nextInt(10));
            sRand += rand;
            g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));
            g.drawString(rand, 13 * i + 6, 16);
        }

        g.dispose();

        return new ImageCode(image, sRand, SecurityConstants.DEFAULT_IMAGE_EXPIRE);
    }

    /**
     * 生成随机背景条纹
     *
     * @param fc
     * @param bc
     * @return
     */
    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255) {
            fc = 255;
        }
        if (bc > 255) {
            bc = 255;
        }
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }
}
